// array for holding data of individuals chats in signature objects

const naved =
 [{ img: "tejas.jpg", 
    name: "Tejas",
    sentmessage: "ho",
    time: "10:00",
    chat: "Udya class la jayche ka Aaj??",
    status: "online",
    chatsend: "ho"},

    { img: "1653080143_modi.jpg", 
    name: "Narendra Modi",
    sentmessage: "tum acche din laana nahi toh aisa hi hoga",
    time: "09:55",
    chat: "Nahi nahi aisa mat krna",
    status: "last seen today at 12.15 pm",
    chatsend: "tum acche din laana nahi toh aisa hi hoga"},

    { img: "download.jpg", 
    name: "Amit Shah",
    sentmessage: "Ladke pr dyaan do kisiko bhi khila raha hai..",
    time: "09:50",
    chat: "Aaj match h",
    status: "last seen today at 11.23 pm",
    chatsend: "Ladke pr dyaan do kisiko bhi khila raha hai.."},

    {img: "ambaani.jpg", 
    name: "Mukesh Ambani",
    sentmessage: "tumhra Jio 5G kabh aa raha hai.",
    time: "09:45",
    chat: "Jio thik chl raha hai na??",
    status: "last seen today at 2.08 pm",
    chatsend: "tumhra Jio 5G kabh aa raha hai."},

    { img: "yogi_ji.jpg", 
    name: "Yogi Ji",
    sentmessage: "kaaho bhaiya UP m sab thik!!!",
    time: "10:00",
    chat: "Pranam",
    status: "online",
    chatsend: "kaaho bhaiya UP m sab thik!!!"},

    { img: "dhoni.jpg", 
    name: "MS Dhoni",
    sentmessage: "Aaj match tumhe hi khtm karna hai",
    time: "10:00",
    chat: "Aaj match khtm karu na??",
    status: "last seen today at 8.08 pm",
    chatsend: "Aaj match tumhe hi khtm karna hai"},

    { img: "virat_k.jpg", 
    name: "Virat Kohli",
    sentmessage: "Century kabh marega bhai??",
    time: "10:00",
    chat: "Aaj match khelna hai",
    status: "online",
    chatsend: "Century kabh marega bhai??"},

    { img: "mom.jpg", 
    name: "Mom",
    sentmessage: "8 baje tak aaunga ghar",
    time: "07:03",
    chat: "kabh aayega ghar??",
    status: "last seen today at 9.05 pm",
    chatsend: "8 baje tak aaunga ghar"},

    { img: "Honey-Singh.jpg", 
    name: "Yo Yo Honey Singh",
    sentmessage: "Bhay song bahoot hard tha",
    time: "07:02",
    chat: "New Song dekha kya??",
    status: "online",
    chatsend: "Bhay song bahoot hard tha"},

    { img: "slamn_khan.jpg", 
    name: "Salman Khan Bhai",
    sentmessage: "Movie M heroine baraber nhi thi..",
    time: "07:00",
    chat: "Movie dekhi kya??",
    status: "online",
    chatsend: "Movie M heroine baraber nhi thi.."},

    { img: "hashmi.jpg", 
    name: "Emraan Hashmi",
    sentmessage: "Song mein macha diya bhaiiii",
    time: "05:55",
    chat: "song sahi hai na",
    status: "online",
    chatsend: "Song mein macha diya bhaiiii"},

    { img: "tendulkar-1200.jpg", 
    name: "Sachin Tendulkar",
    sentmessage: "Mumbai Indians kadhi sodtoy??",
    time: "05:30",
    chat: "Yeh baar bhi mumbau hi win hui",
    status: "online",
    chatsend: "Mumbai Indians kadhi sodtoy??"},

    { img: "images_neeta.jpg", 
    name: "Neeta Ambaani",
    sentmessage: "Thoda sa paisa bhej do behn",
    time: "05:23",
    chat: "Kaise ho bhai",
    status: "online",
    chatsend: "Thoda sa paisa bhej do behn"},

    { img: "siddesh.jpg", 
    name: "Siddesh",
    sentmessage: "Tu kadhi janar ahes class la",
    time: "04:00",
    chat: "Aaj yenar ahes ka class la??",
    status: "last seen today at 12.15 pm",
    chatsend: "Tu kadhi janar ahes class la"},

    { img: "rahul_gandhi.jpg", 
    name: "Rahul Gandhi",
    sentmessage: "Vaha modi ji macha rahe h tumhare kabh din aayenge",
    time: "03:55",
    chat: "Yeh baar hume hi aana hai",
    status: "last seen today at 4.59 pm",
    chatsend: "Vaha modi ji macha rahe h tumhare kabh din aayenge"},

    { img: "jaid.jpg", 
    name: "Jaid",
    sentmessage: "kabh aane vala hai",
    time: "03:43",
    chat: "Hi",
    status: "online",
    chatsend: "kabh aane vala hai"},

    { img: "sagar.jpg", 
    name: "Sagar Bhai",
    sentmessage: "class ko chutti he na??",
    time: "02:44",
    chat: "hi",
    status: "online",
    chatsend: "class ko chutti he na??"},

    { img: "arsh.jpg", 
    name: "Arsh",
    sentmessage: "ghar aa jaldi..",
    time: "02:00",
    chat: "Late hoga",
    status: "online",
    chatsend: "ghar aa jaldi.."},
 ]

 // variable text which stores chats html
 var text = "";

 // for loop thats displays every chat multiple times
 for (let i = 0; i<naved.length; i++)  {
text = text +

`<div class="sidebar-chat" onclick=myfunction(${i})>
<div class="chat-avatar">
    <img src=${naved[i].img} alt=${i} + "chat dp">
    </div>
    <div class="chat-info">
    <h4>${naved[i].name}.</h4>
    <p>${naved[i].sentmessage}</p>
    </div>
    <div class="time">
    <p>${naved[i].time}</p>
    </div>
    </div>`
 }

 //this statements calls the chats container div and fills the above for loop content as html in the container

 document.getElementById('sidebar-chats').innerHTML = text; 
 
// the given function is used to change the background color of object when clicked  
function myfunction(selected) {
   function clickChange(selected) {
     className = "sidebar-chat onclick";
     return className;
   }
 
 
   var text = "";
   for (i = 0; i < naved.length; i++) {
     text = text + `<div class="${selected == i ? clickChange(selected) : "sidebar-chat"}"  onclick=myfunction(${i})>
   <div class="chat-avatar">
       <img src=${naved[i].img} alt="">
   </div>
 
   <div class="chat-info">
       <h4>${naved[i].name}</h4>
       <p>${naved[i].sentmessage}</p>
   </div>
   <div class="time">
       <p>${naved[i].time}</p>
   </div>
 </div>`
   }
 
   document.getElementById('sidebar-chats').innerHTML = text;
 
   document.getElementById("header").innerHTML = `
 
  
   <div class="chat-title">
       <div class="avatar">
           <img src="${naved[selected].img}" alt="">
       </div>
       <div class="message-header-content">
           <h4>${naved[selected].name}</h4>
           <p>${naved[selected].status}</p>
       </div>
   </div>
   <div class="chat-header-right">
               <img src="search.svg" alt="">
               <img src="more.svg" alt="">
           </div>`;
 
   document.getElementById("message-content").innerHTML = `
 
  
           <p class="chat-message">${naved[selected].chat}<span class="chat-timestamp">${naved[selected].time}</span></p> 
           
           <p class="chat-message chat-sent">${naved[selected].chatsend}<span class="chat-timestamp">${naved[selected].time}</span></p>`;
 

 // responsive using js
if(window.innerWidth<="950"){
   document.getElementById("sidebar").style.display="none";
   document.getElementById("message-container").style.display="block";
   }else{
       document.getElementById("sidebar").style.display="block";
       document.getElementById("message-container").style.display="block";
   }
}



